package protocol

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"os"
	"testing"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/api/types/network"
	docker "github.com/docker/docker/client"
	"github.com/docker/go-connections/nat"
	"github.com/google/uuid"
	. "gopkg.in/check.v1"
)

const (
	testPrefix  = "/test"
	appPrefix   = "/app"
	networkName = "testnetwork"
	serverImage = "testserver"
	serverName  = serverImage
	clientImage = "testclient"
	clientFile  = appPrefix + "/client/main.js"
	testPort    = "8080"
	mountDir    = appPrefix
	// Environment variable that describes the current dir from outside the
	// container. This can be used to share a directory between the test
	// container and the test client
	testDirEnv  = "TESTDIR"
	resultFile1 = "/result1"
	resultFile2 = "/result2"
)

type ClientResult struct {
	Sequence       []int `json:"sequence"`
	Sum            int   `json:"sum"`
	Matched        bool  `json:"matched"`
	ClientChecksum int   `json:"client_checksum"`
	ServerChecksum int   `json:"server_checksum"`
}

func Test(t *testing.T) { TestingT(t) }

var _ = Suite(&ProtocolTestSuite{})

type ProtocolTestSuite struct {
	ctx        context.Context
	dkrClient  *docker.Client
	networkID  string
	networkCfg *network.NetworkingConfig
	serverID   string
	clientIDs  []string
}

// SetUpSuite called at the start of the test suite
func (s *ProtocolTestSuite) SetUpSuite(c *C) {
	var err error
	s.ctx = context.Background()
	s.dkrClient, err = docker.NewClientWithOpts(docker.FromEnv, docker.WithAPIVersionNegotiation())
	c.Assert(err, IsNil)

	// Create the test network
	netOptions := types.NetworkCreate{Attachable: true}
	resp, err := s.dkrClient.NetworkCreate(s.ctx, networkName, netOptions)
	c.Assert(err, IsNil)
	s.networkID = resp.ID

	// Start the server container
	s.networkCfg = &network.NetworkingConfig{
		EndpointsConfig: map[string]*network.EndpointSettings{
			"network1": &network.EndpointSettings{
				NetworkID: s.networkID,
			},
		},
	}
	serverCfg := &container.Config{
		Hostname:     serverName,
		Image:        serverImage,
		Cmd:          []string{testPort},
		ExposedPorts: nat.PortSet{"8080:8080/tcp": struct{}{}},
	}
	contResp, err := s.dkrClient.ContainerCreate(s.ctx, serverCfg, nil, s.networkCfg, serverName)
	c.Assert(err, IsNil)
	err = s.dkrClient.ContainerStart(s.ctx, contResp.ID, types.ContainerStartOptions{})
	c.Assert(err, IsNil)
	s.serverID = contResp.ID

}

// TearDownSuite cleans up at the end of the test suite
func (s *ProtocolTestSuite) TearDownSuite(c *C) {
	err := s.dkrClient.ContainerStop(s.ctx, s.serverID, nil)
	c.Assert(err, IsNil)
	err = s.dkrClient.ContainerRemove(s.ctx, s.serverID, types.ContainerRemoveOptions{})
	c.Assert(err, IsNil)
	err = s.dkrClient.NetworkRemove(s.ctx, s.networkID)
	c.Assert(err, IsNil)
	err = s.dkrClient.Close()
	c.Assert(err, IsNil)
}

// SetUpTest performs per-test setup
func (s *ProtocolTestSuite) SetUpTest(c *C) {
	s.clientIDs = []string{}
	os.Remove(testPrefix + resultFile1)
	os.Remove(testPrefix + resultFile2)
}

// TearDownTest performs per-test cleanup
func (s *ProtocolTestSuite) TearDownTest(c *C) {
	for _, id := range s.clientIDs {
		s.dkrClient.ContainerStop(s.ctx, id, nil)
		s.dkrClient.ContainerRemove(s.ctx, id, types.ContainerRemoveOptions{})
	}
}

func (s *ProtocolTestSuite) startClient(cfg *container.Config, clientName string, c *C) string {
	mounts := []mount.Mount{mount.Mount{
		Type:   mount.TypeBind,
		Source: os.Getenv("TESTDIR"),
		Target: mountDir,
	},
	}
	hostCfg := &container.HostConfig{
		Mounts: mounts,
	}
	resp, err := s.dkrClient.ContainerCreate(s.ctx, cfg, hostCfg, s.networkCfg, clientName)
	c.Assert(err, IsNil)
	err = s.dkrClient.ContainerStart(s.ctx, resp.ID, types.ContainerStartOptions{})
	c.Assert(err, IsNil)
	s.clientIDs = append(s.clientIDs, resp.ID)
	return resp.ID
}

func (s *ProtocolTestSuite) waitForResultFile(resultPath string, timeout int, c *C) {
	for i := 0; i < timeout; i++ {
		if _, err := os.Stat(resultPath); os.IsNotExist(err) {
			time.Sleep(1 * time.Second)
			continue
		} else {
			return
		}
	}
	c.Fatalf("Result file not found after %d s\n", timeout)
}

func (s *ProtocolTestSuite) checkNoResultFile(resultPath string, timeout int, c *C) {
	for i := 0; i < timeout; i++ {
		if _, err := os.Stat(resultPath); os.IsNotExist(err) {
			time.Sleep(1 * time.Second)
			continue
		}
	}
}

// readResultFile reads the result from the result file
func (s *ProtocolTestSuite) readResultFile(resultPath string, c *C) ClientResult {
	f, err := os.Open(resultPath)
	c.Assert(err, IsNil)
	data, err := ioutil.ReadAll(f)
	c.Assert(err, IsNil)
	var res ClientResult
	err = json.Unmarshal(data, &res)
	c.Assert(err, IsNil)
	return res
}

// checkStatelessResult check the sequence is what was expected
func (s *ProtocolTestSuite) checkStatelessResult(res ClientResult, c *C) {
	if res.Sequence[0] < 1 || res.Sequence[0] > 255 {
		c.Fatalf("Initial value (%d) is not between expected values", res.Sequence[0])
	}

	sum := 0
	for i, val := range res.Sequence {
		sum += val
		if i == 0 {
			continue
		}

		if val != res.Sequence[i-1]*2 {
			c.Fatalf("Current value (%d) is not twice the previous (%d)",
				val, res.Sequence[i-1])
		}
	}
	c.Check(res.Sum, Equals, sum)
}

func (s *ProtocolTestSuite) restartContainer(containerID string, waitTime time.Duration, c *C) {
	err := s.dkrClient.ContainerStop(s.ctx, containerID, nil)
	c.Assert(err, IsNil)
	err = s.dkrClient.NetworkDisconnect(s.ctx, s.networkID, containerID, false)
	c.Assert(err, IsNil)
	time.Sleep(waitTime)
	err = s.dkrClient.ContainerStart(s.ctx, containerID, types.ContainerStartOptions{})
	c.Assert(err, IsNil)
}

// TestStatelessSuccess tests that if the client connects in a stateless mode
// a valid value is obtained from the client
func (s *ProtocolTestSuite) TestStatelessSuccess(c *C) {
	// Given - The client is configured in stateless mode
	resultFile := testPrefix + resultFile1
	clientName := "testclient"
	clientCfg := &container.Config{
		Image: clientImage,
		Cmd: []string{clientFile, "-p", "8080", "-a", serverName, "-s",
			"false", "-m", "10", "-f", appPrefix + resultFile1},
		ExposedPorts: nat.PortSet{"8080:8080/tcp": struct{}{}},
	}
	s.startClient(clientCfg, clientName, c)

	// When - Wait for the client to receive all the message
	s.waitForResultFile(resultFile, 15, c)

	// Then - Expect the output to show the client received all the messages
	// and the calculated value is sensible
	res := s.readResultFile(resultFile, c)
	s.checkStatelessResult(res, c)
}

// TestStatelessMultipleClient tests the server can handle multiple simultaneous
// connected clients
func (s *ProtocolTestSuite) TestStatelessMultipleClient(c *C) {
	// Given - 2 clients are configured and started
	clientCfg1 := &container.Config{
		Image: clientImage,
		Cmd: []string{clientFile, "-p", "8080", "-a", serverName, "-s",
			"false", "-m", "10", "-f", appPrefix + resultFile1},
		ExposedPorts: nat.PortSet{"8080:8080/tcp": struct{}{}},
	}
	clientCfg2 := &container.Config{
		Image: clientImage,
		Cmd: []string{clientFile, "-p", "8080", "-a", serverName, "-s",
			"false", "-m", "10", "-f", appPrefix + resultFile2},
		ExposedPorts: nat.PortSet{"8080:8080/tcp": struct{}{}},
	}
	s.startClient(clientCfg1, "testclient1", c)
	s.startClient(clientCfg2, "testclient2", c)

	// When - Wait for the client to receive all the message
	s.waitForResultFile(testPrefix+resultFile2, 15, c)

	// Then - Expect the output to show the client received all the messages
	// and the calculated value is sensible
	res := s.readResultFile(testPrefix+resultFile1, c)
	s.checkStatelessResult(res, c)
	res = s.readResultFile(testPrefix+resultFile2, c)
	s.checkStatelessResult(res, c)
}

// TestStatelessServerRestart tests that if the connection between client and server
// is interrupted the client can resume once it reconnects
func (s *ProtocolTestSuite) TestStatelessServerRestart(c *C) {
	// Given - The client is configured in stateless mode
	resultFile := testPrefix + resultFile1
	clientName := "testclient"
	clientCfg := &container.Config{
		Image: clientImage,
		Cmd: []string{clientFile, "-p", "8080", "-a", serverName, "-s",
			"false", "-m", "10", "-f", appPrefix + resultFile1},
		ExposedPorts: nat.PortSet{"8080:8080/tcp": struct{}{}},
	}
	s.startClient(clientCfg, clientName, c)
	// Give the client some time to start receiving
	time.Sleep(2 * time.Second)

	// When - The server container is restart and the client is allowed to reconnect
	s.restartContainer(s.serverID, 2*time.Second, c)

	// Then - Wait for the result and check the output to show the client
	// received all the messages and the values are sensible
	s.waitForResultFile(resultFile, 15, c)
	res := s.readResultFile(resultFile, c)
	s.checkStatelessResult(res, c)
}

// TestStatefulSuccess tests that in stateful mode the client receives
// a valid sequence of messages
func (s *ProtocolTestSuite) TestStatefulSuccess(c *C) {
	// Given - The client is configured in stateful mode
	resultFile := testPrefix + resultFile1
	clientName := "testclient"
	clientCfg := &container.Config{
		Image: clientImage,
		Cmd: []string{clientFile, "-p", "8080", "-a", serverName, "-s",
			"true", "-m", "10", "-f", appPrefix + resultFile1},
		ExposedPorts: nat.PortSet{"8080:8080/tcp": struct{}{}},
	}
	s.startClient(clientCfg, clientName, c)

	// When - Wait for the client to receive all the message
	s.waitForResultFile(resultFile, 20, c)

	// Then - Expect the output to show the client received all the messages
	res := s.readResultFile(resultFile, c)
	c.Check(res.Matched, Equals, true)
	c.Check(res.ClientChecksum, Equals, res.ServerChecksum)
}

// TestStatefulMultipleClients tests multiple stateful clients connecting to the
// same server
func (s *ProtocolTestSuite) TestStatefulMultipleClients(c *C) {
	// Given - 2 clients are configured and started
	clientCfg1 := &container.Config{
		Image: clientImage,
		Cmd: []string{clientFile, "-p", "8080", "-a", serverName, "-s",
			"true", "-m", "10", "-f", appPrefix + resultFile1},
		ExposedPorts: nat.PortSet{"8080:8080/tcp": struct{}{}},
	}
	clientCfg2 := &container.Config{
		Image: clientImage,
		Cmd: []string{clientFile, "-p", "8080", "-a", serverName, "-s",
			"true", "-m", "10", "-f", appPrefix + resultFile2},
		ExposedPorts: nat.PortSet{"8080:8080/tcp": struct{}{}},
	}
	s.startClient(clientCfg1, "testclient1", c)
	s.startClient(clientCfg2, "testclient2", c)

	// When - Wait for the result files to be written
	s.waitForResultFile(testPrefix+resultFile2, 20, c)
	s.waitForResultFile(testPrefix+resultFile1, 20, c)

	// Then - Expect the output to show the client received all the messages
	res := s.readResultFile(testPrefix+resultFile1, c)
	c.Check(res.Matched, Equals, true)
	c.Check(res.ClientChecksum, Equals, res.ServerChecksum)
	res = s.readResultFile(testPrefix+resultFile2, c)
	c.Check(res.Matched, Equals, true)
	c.Check(res.ClientChecksum, Equals, res.ServerChecksum)
}

// TestStatefulReconnect tests that if a client reconnects the session resumes
func (s *ProtocolTestSuite) TestStatefulReconnect(c *C) {
	// Given - The client is configured in stateful mode
	clientName := "testclient"
	clientID := uuid.New().String()
	clientCfg := &container.Config{
		Image: clientImage,
		Cmd: []string{clientFile, "-p", "8080", "-a", serverName, "-s",
			"true", "-m", "10", "-f", appPrefix + resultFile1, "i", clientID},
		ExposedPorts: nat.PortSet{"8080:8080/tcp": struct{}{}},
	}
	s.startClient(clientCfg, clientName, c)
	time.Sleep(2 * time.Second)

	// When - The client is restarte
	s.restartContainer(s.clientIDs[0], 2*time.Second, c)

	// Then - Expect no result file to be created
	s.waitForResultFile(testPrefix+resultFile1, 15, c)
	res := s.readResultFile(testPrefix+resultFile1, c)
	c.Check(res.Matched, Equals, true)
	c.Check(res.ClientChecksum, Equals, res.ServerChecksum)
}

// TestStatefulReconnect30s tests that if a client reconnects after 30s
// the session is no longer valid
func (s *ProtocolTestSuite) TestStatefulReconnect30s(c *C) {
	// Given - The client is configured in stateful mode
	clientName := "testclient"
	clientID := uuid.New().String()
	clientCfg := &container.Config{
		Image: clientImage,
		Cmd: []string{clientFile, "-p", "8080", "-a", serverName, "-s",
			"true", "-m", "10", "-f", appPrefix + resultFile1, "i", clientID},
		ExposedPorts: nat.PortSet{"8080:8080/tcp": struct{}{}},
	}
	s.startClient(clientCfg, clientName, c)

	// When - The client is stopped and enough time elapses for it to expire
	s.restartContainer(s.clientIDs[0], 31*time.Second, c)

	// Then - Expect no result file to be created
	s.checkNoResultFile(testPrefix+resultFile1, 10, c)
}
