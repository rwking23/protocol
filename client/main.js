const yargs = require('yargs');
const Client =require('./client');

const argv = yargs
    .option('port', {
      alias: 'p',
      description: 'Port to connect to',
      type: 'number',
  }).option('address', {
      alias: 'a',
      description: 'Address to connect to',
      type: 'string',
  }).option('stateful', {
      alias: 's',
      description: 'Whether the client is stateful or not',
      type: 'boolean',
  }).option('max', {
    alias: 'm',
    description: 'Max number of records to receive',
    type: 'number',
  }).option('file', {
    alias: 'f',
    description: 'Result file path',
    type: 'string',
  }).option('id', {
    alias: 'i',
    description: 'Client ID',
    type: 'string',
  }).help()
    .alias('help', 'h')
    .argv;

console.log('Supplied args', argv);
new Client(argv.port, argv.address, argv.stateful, argv.max, argv.file, argv.id);