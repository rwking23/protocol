'use strict';
const net = require('net');
const uuid = require('uuid/v1');
const rand = require('random-number-in-range')
const fs = require('fs');

const resultFile = "/app/result"

/**
 * Client provides a TCPClient that implements the protocol sending
 * and receiving data via JSON. The client can be invoked in either a
 * stateless or stateful mode depending on the value of the stateful variable
 */
class Client {
  /**
   * Creates a new client using the provided variables
   * @param {number} port TCP port to connect to
   * @param {string} address TCP Address to connect to
   * @param {boolean} stateful whether the client is being started in a stateful mode
   * @param {number} maxReceived Limits the number of messages to receive
   * @param {string} resultFile Path to the result file
   */
  constructor(port, address, stateful, maxReceived, resultFile, id) {
    this.maxReceived = maxReceived;
    this.stateful = stateful;
    this.socket = new net.Socket();
    this.address = address;
    this.port = port;
    this.sequence = [];
    this.received = 0;
    this.checksum = 0;
    this.id = id || "";
    this.done = false;
    this.resultFile = resultFile;
    this.run();
  }

  /**
   * Handles responses from a stateful connection the last message in the sequence
   * is expected to contain a checksum which the client can use to verify all the
   * messages have been correctly received
   * @param {array} data the received message
   */
  handleStateful(data) {
    let msg = JSON.parse(data);
    let matched = false;
    if (msg.checksum != "") {
      if (msg.checksum == this.checksum) {
        matched = true;
      };
      this.writeResult({
        "matched": matched,
        "client_checksum": this.checksum,
        "server_checksum": msg.checksum,
      });
      this.socket.end();
      this.socket.destroy();
    } else {
      this.checksum += data.length;
    }
  };

  /**
   * Handles responses from a stateless connection receiving up to maxReceived
   * messages. Once all have been received the summation of the sequence is
   * written out to the result file.
   * @param {array} data the received message
   */
  handleStateless(data) {
    let msg = JSON.parse(data);
    this.sequence.push(msg.value);
    this.received++;
    if (this.received == this.maxReceived) {
      let sum = this.sequence.reduce((a, b) => a + b)
      this.writeResult({
        "sequence": this.sequence,
        "sum": sum,
      })
      this.socket.end();
      this.socket.destroy();
    }
  };
  
  /**
   * Writes the supplied result object to the result file
   * @param {object} result 
   */
  writeResult(result) {
    this.done = true;
    let jsonResult = JSON.stringify(result);
    fs.writeFile(this.resultFile, `${jsonResult}`, function(err) {
      if(err) {
          return console.log(err);
      }
      console.log(`Result ${result} written to file`);
    });
  }; 

  /**
   * Connects to the TCP endpoint described by address and port
   * and sends the connect message on a successful connection
   * @param {msg} msg 
   */
  connect(msg) {
    this.socket.connect(this.port, this.address, () => {
      console.log(`Client connected to: ${this.address}:${this.port}`);
      let strMsg = JSON.stringify(msg);
      this.socket.write(strMsg);
    });
  }

  run() {
    let msg = {"start_value": 0}
    if (this.stateful) {
      if (this.id == "") {
        this.id = uuid();
      }
      msg = {
        "id": this.id,
        "sequence_length": rand(1, this.maxReceived)
      }
    }
    this.connect(msg)
 
    this.socket.on('data', (data) => {
      if (this.stateful) {
        this.handleStateful(data);
      } else {
        this.handleStateless(data)
      }
    });
  
    this.socket.on('connect', () => {
      console.log('Client connected');
    });

    this.socket.on('close', () => {
      console.log('Client closed');
      if (!this.done) {
        // Send an initial message if in stateless mode the next expected value
        // is supplied and the ID will be empty. If in stateful mode then the 
        // value will be empty but the ID will be supplied
        let msg = {"id": this.id,
                  "start_value": this.sequence[this.sequence.length-1]*2
                  };
        setTimeout(() => {
          this.connect(msg);
        }, 2000);
      } else {
        this.socket.end();
        this.socket.destroy();
      }
    });
  
    this.socket.on('error', (err) => {
      console.error(err);
    });

    this.socket.on('end', () => {
      this.socket.destroy();
      console.error('Connection ended');
    }); 
  }
}
module.exports = Client;