package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/rwking23/protocol/server"
)

func main() {
	flag.Parse()
	args := flag.Args()

	if len(args) < 1 {
		log.Fatalf("No port argument provided\n")
	}
	port := args[0]

	sessionCache := server.NewSessionCache()
	protoServer, err := server.NewServer(":"+port, sessionCache)
	if err != nil {
		log.Fatalf("Error creating new client: %s", err)
	}

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)

	protoServer.Listen()
	log.Printf("Server running\n")
	<-sigChan
	sessionCache.Close()
	protoServer.Stop()
	log.Printf("Server stopped\n")
}
