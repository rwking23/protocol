# Protocol

This repository defines a protocol that can be used to send messages in JSON format over TCP. Allowing a client to connect to a server and receive a stream of messages in response each containing 1 value of a sequence. The protocol allows a client and server to connect in either a stateless or stateful manner.

## Messages
`protocol.json` defines 2 types of messages `Connect` this is the message the client sends to the server after establishing a connection. The second is the `Sequence` message which is sent from server to client and contains the 
sequence of numbers the server generates.

## State
The reference implementations in this repository provide both stateless and stateful variants. The client can be supplied parameters which it uses to decide whether it should be stateful or not use the -h option to see what parameters are available. The server uses the client connection parameters to infer whether it should retain the client state or not.

## Modes

### Stateless Mode
In stateless mode the client sends the initial connect message in either an empty state or with a 0 start_value to indicate to the server it should generate a new sequence start value. If the connection drops then when the client has established a new connection it should send another connect message with the last successfully received value.

### Stateful Mode
In stateful mode the client connects using the connect message supplying an ID in the form of a UUID and a sequence length. The server stores the clients state. If the client gets disconnected whilst receiving the sequence it can resume the sequence from where it left off as long as it reconnects within 30s. To resume client must reconnect supplying the original ID. The client knows when the sequence is complete when it receives the message with the checksum and can verify by adding the size of all previous messages and comparing.

**PRNG**
The number generator in use is the Go random number generator from the rand package. It is seeded with a psuedo-random source https://golang.org/pkg/math/rand/#NewSource which itself has been initialised using a randomly generated number.

**Checksum**
The checksum used is a simple sum of bytes checksum where the total bytes for each messages are summed together. This is quick and easy to calculate and although potentially has some flaws in the errors that can be detected is potentially sufficient for now.

**Session Store**
The session store provides an API `SessionStore` as defined in session.go. This API could be used to wrap another external store like Redis without exposing any change in functionality to the server. The provided implementation the `SessionCache` stores a `Session` object against the client ID. The `Session` object contains the the generated sequence and the current sum. These are the minimum required to restore the session, as the client provides the last received value so the sequence can be searched for the last know position and the sum keeps track of total number of bytes so far. On top of this the time to expire and whether it has been expired are all stored to help with the expiry of sessions.

# Build & Run

Docker containers have been supplied for both the client and server. To build the relevant containers run the following commands at the top of the checkout.

client `docker build -t testclient -f Dockerfile-client .`

server `docker build -t testserver -f dockerfile-server .`

To run then use

`docker run --rm -it testserver` & `docker run --rm -it testclient` 

Alternatively a docker-compose file has been supplied to run them both, run:

`docker-compose up --build` to build and run both services.

# Test
Full stack test have been written in `protocol_test.go` to test the reference implementations of the client and server using the docker containers. Along with these some unit tests have been written for the client cache used by the server to store client state. To run all the tests first build the containers as described above then run

`run_tests.sh`

The full stack tests start a test container mounting the docker socket inside. the tests then use the Go docker client to control the server and client containers.

For testing purposes additional parameters have been added to the command line of the client to allow certain variables to be fixed for deterministic testing.

Individual tests can be run by modifying the arguments to the docker run command and adding `"-check.f", "<test name>"`


