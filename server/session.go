package server

import (
	"fmt"
	"log"
	"sync"
	"time"
)

const expiryCheck = 500 * time.Millisecond

// SessionStore provides an interface that can be used to wrap an implementation
// of a SessionStore. The underlying store could be external to the application
// or builtin.
type SessionStore interface {
	// Add adds a new session to the store, any error encountered will be returned
	// and the session wont get added.
	Add(key string, session Session) error
	// Set sets the value of the session pointed at by key to the session provided
	Set(key string, session Session) error
	// Get retrieves the session pointed at by the key
	Get(key string) (session Session, err error)
	// SetTTL sets an expiry time on the session pointed at by key to the provided
	// TTL duration
	SetTTL(key string, ttl time.Duration) error
	// Delete deletes the session pointed at by the key
	Delete(key string) error
	// Close cleans up any resources managed by the SessionStore
	Close()
}

// Session defines the information important for each session
type Session struct {
	sequence   []uint32
	currentSum uint32
	expireAt   time.Time
	expired    bool
}

// SessionCache implements the SessionStore interface and can be used
// for storing sessions in memory. If a TTL is set on a stored session then the
// cache will automatically expire the session after the provided time.
// The cache is thread safe and can be accessed from multiple go routines.
type SessionCache struct {
	storeLock *sync.Mutex
	store     map[string]*Session
	closeChan chan bool
}

// Compile time check that the SessionCache implements the SessionStore
var _ SessionStore = (*SessionCache)(nil)

// NewSessionCache creates a new SessionCache and starts a background goroutine
// check for expired session
func NewSessionCache() SessionStore {
	cache := &SessionCache{
		storeLock: &sync.Mutex{},
		store:     make(map[string]*Session),
		closeChan: make(chan bool),
	}
	go cache.expiryChecker()
	return cache
}

// Add adds a new session to the store any errors like the session already existing
// will be returned
func (s *SessionCache) Add(key string, session Session) error {
	s.storeLock.Lock()
	defer s.storeLock.Unlock()
	if _, ok := s.store[key]; ok {
		return fmt.Errorf("Session %s already exists", key)
	}
	s.store[key] = &session
	return nil
}

// Set sets the session pointed at by the key to contain the new session item
// provided. If the session does not exist an error is returned
func (s *SessionCache) Set(key string, session Session) error {
	s.storeLock.Lock()
	defer s.storeLock.Unlock()
	if _, ok := s.store[key]; !ok {
		return fmt.Errorf("Session with (%s) does not exist", key)
	}
	s.store[key] = &session
	return nil
}

// Get returns the session pointed at by the key, if the session does not exist
// an error is returned
func (s *SessionCache) Get(key string) (Session, error) {
	s.storeLock.Lock()
	defer s.storeLock.Unlock()
	session, ok := s.store[key]
	if !ok {
		return Session{}, fmt.Errorf("Session with (%s) does not exist", key)
	}
	return *session, nil
}

// Delete will delete the session pointed at by the key. This method will
// always return nil as it can't fail but the error is required to satisfy
// the interface
func (s *SessionCache) Delete(key string) error {
	s.storeLock.Lock()
	defer s.storeLock.Unlock()
	delete(s.store, key)
	return nil
}

// SetTTL sets the time to live on the Session pointed at by the key
func (s *SessionCache) SetTTL(key string, ttl time.Duration) error {
	s.storeLock.Lock()
	defer s.storeLock.Unlock()
	session, ok := s.store[key]
	if !ok {
		return fmt.Errorf("Session with (%s) does not exist", key)
	}
	session.expireAt = time.Now().Add(ttl)
	fmt.Printf("SetTTL = %s\n", session.expireAt)
	return nil
}

// Close closes the SessionCache and stops the background expiry checker
func (s *SessionCache) Close() {
	close(s.closeChan)
}

func (s *SessionCache) expire(id string) error {
	session, err := s.Get(id)
	if err != nil {
		return err
	}
	session.sequence = []uint32{}
	session.currentSum = 0
	session.expired = true
	if err := s.Set(id, session); err != nil {
		return err
	}
	log.Printf("Expired %s\n", id)
	return nil
}

func (s *SessionCache) expiryChecker() {
	for {
		s.storeLock.Lock()
		for key, value := range s.store {
			if value.expired {
				continue
			}

			if value.expireAt.After(time.Now()) {
				s.storeLock.Unlock()
				if err := s.expire(key); err != nil {
					log.Printf("Failed to expire client\n")
				}
				s.storeLock.Lock()
			}
		}
		s.storeLock.Unlock()

		select {
		case <-s.closeChan:
			return
		default:
			// wait before checking for expired session
			time.Sleep(expiryCheck)
		}
	}
}
