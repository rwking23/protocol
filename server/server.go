package server

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net"
	"time"

	"gitlab.com/rwking23/protocol"
)

const (
	multiplier = 2
	waitTime   = 1 * time.Second
	sessionTTL = 30 * time.Second
)

// Server implements a server to handle the messaging protocol over a TCP
// connection. It can handle both stateless and statefull connections with
// the state for the connections being retained in the SessionStore ofr up to
// the time specified by the TTL.
type Server struct {
	listener     net.Listener
	sessionStore SessionStore
	stop         chan bool
}

// NewServer creates a new server for the protocol listening on the supplied
// address. If a client connects in a stateful mode then the session
// is stored in the supplied session store
func NewServer(addr string, store SessionStore) (*Server, error) {
	listener, err := net.Listen("tcp", addr)
	if err != nil {
		return nil, err
	}
	return &Server{
		listener:     listener,
		sessionStore: store,
		stop:         make(chan bool),
	}, nil
}

// Listen starts the server listening in a new goroutine
func (s *Server) Listen() {
	go s.listen()
}

// Stop stops the listener
func (s *Server) Stop() {
	close(s.stop)
}

// listen will accept new connections and spawns a new goroutine to handle the
// connection
func (s *Server) listen() {
	for {
		select {
		case <-s.stop:
			log.Printf("Stopping listener\n")
			s.listener.Close()
			return
		default:
		}

		conn, err := s.listener.Accept()
		if err != nil {
			fmt.Printf("Error accepting the TCP connection: %s", err)
		}
		go s.handleConnection(conn.(*net.TCPConn))
	}
}

func (s *Server) checkConnClosed(conn *net.TCPConn, notify chan error) {
	data := make([]byte, 1024)
	for {
		_, err := conn.Read(data)
		if err != nil {
			notify <- err
			return
		}
	}
}

// handleConnection uses the ID from the Connect message received after the
// connection to decide the client has connected in a stateless or staeful
// manner
func (s *Server) handleConnection(conn *net.TCPConn) {
	defer conn.Close()
	decoder := json.NewDecoder(conn)
	msg := protocol.Connect{}
	if err := decoder.Decode(&msg); err != nil {
		return
	}
	if msg.ID == "" {
		s.handleStatelessConnection(conn, msg)
	} else {
		s.handleStatefulConnection(conn, msg)
	}
}

// handleStatelessConnection handles a stateless connection
func (s *Server) handleStatelessConnection(conn *net.TCPConn, msg protocol.Connect) error {
	log.Printf("Stateless connection\n")
	return s.generateAndSendSequence(conn, msg.StartValue)
}

// handleStatefulConnection handles a connection that includes state from the client.
// If the client has previously been connected and there is a stored session. The
// session will be resumed, if this is the first connection a new sequence will be
// generated with a length defined by the client.
func (s *Server) handleStatefulConnection(conn *net.TCPConn, msg protocol.Connect) error {
	log.Printf("Stateful connection with ID %s length %d\n", msg.ID, msg.SequenceLength)
	session, err := s.sessionStore.Get(msg.ID)
	// New session so add it to the store
	if err != nil {
		sequence := s.generateSequence(msg.SequenceLength)
		session = Session{
			sequence:   sequence,
			currentSum: 0,
		}
		s.sessionStore.Add(msg.ID, session)
	}

	// Session has expired log and return the error to close the connection
	if session.expired {
		err := fmt.Errorf("Session has expired")
		log.Printf("%s\n", err)
		return err
	}

	if err = s.sendSequence(conn, msg.ID, session); err != nil {
		log.Printf("Error sending the sequence: %s", err)
		return err
	}
	s.sessionStore.Delete(msg.ID)
	return nil
}

// sendSequence sends the provided sequence within the Session over the TCP connection
// it calculates a sum of bytes checksum as it goes. If an error occurs the session
// state is stored this includes the remainding unsent numbers in the sequence and the
// current sum for the checksum.
func (s *Server) sendSequence(conn *net.TCPConn, sessionID string, session Session) error {
	sum := uint32(0)
	notify := make(chan error)
	checksum := uint32(0)
	go s.checkConnClosed(conn, notify)
	for index, value := range session.sequence {
		// Detect closed conn
		select {
		case err := <-notify:
			session.currentSum = sum
			session.sequence = session.sequence[index:]
			s.sessionStore.Set(sessionID, session)
			s.sessionStore.SetTTL(sessionID, sessionTTL)
			log.Printf("Detected closed conn\n")
			return err
		default:
		}
		if index == len(session.sequence)-1 {
			checksum = sum
		}

		msg := protocol.Sequence{
			Value:    int(value),
			Checksum: checksum,
		}

		encodedMsg, err := json.Marshal(&msg)
		if err != nil {
			return err
		}
		n, err := conn.Write(encodedMsg)
		if err != nil {
			log.Printf("Error writing sequence, setting client to expire: %s", err)
			return err
		}
		sum += uint32(n)
		time.Sleep(waitTime)
	}
	return nil
}

// generateAndSendSequence if the provided startValue is 0 then a random start
// value is generated and and sequence formed by continually multiplying this by 2
// and sending the new value on the connection after 1 second
func (s *Server) generateAndSendSequence(conn *net.TCPConn, startValue int) error {
	rand.Seed(time.Now().UnixNano())
	initialValue := 0
	if startValue != 0 {
		initialValue = startValue
	} else {
		initialValue = int(generateValue())
	}

	sequenceValue := initialValue
	encoder := json.NewEncoder(conn)
	notify := make(chan error)
	go s.checkConnClosed(conn, notify)
	for {
		// Detect closed conn
		select {
		case err := <-notify:
			log.Printf("Detected closed conn\n")
			return err
		default:
		}

		msg := protocol.Sequence{
			Value: int(sequenceValue),
		}
		if err := encoder.Encode(&msg); err != nil {
			return err
		}
		sequenceValue *= multiplier
		time.Sleep(waitTime)
	}
}

func (s *Server) generateSequence(length int) []uint32 {
	randSeed := generateValue()
	source := rand.New(rand.NewSource(randSeed))
	sequence := make([]uint32, 0)
	for i := 0; i < length; i++ {
		sequence = append(sequence, source.Uint32())
	}
	return sequence
}

// generateValue generates a new in64 value
func generateValue() int64 {
	rand.Seed(time.Now().UnixNano())
	value := rand.Int63n(255)
	if value == int64(0) {
		value++
	}
	return value
}
