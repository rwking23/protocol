package server

import (
	"testing"
	"time"

	"github.com/google/uuid"

	. "gopkg.in/check.v1"
)

func Test(t *testing.T) { TestingT(t) }

var _ = Suite(&SessionCacheTestSuite{})

type SessionCacheTestSuite struct {
	testCache SessionStore
}

func (s *SessionCacheTestSuite) SetUpTest(c *C) {
	s.testCache = NewSessionCache()
}

func (s *SessionCacheTestSuite) TearDownTest(c *C) {
	s.testCache.Close()
	s.testCache = &SessionCache{}
}

func newSession() (string, Session) {
	return uuid.New().String(), Session{}
}

// addSession adds the provided session to the  default cache
func (s *SessionCacheTestSuite) addSession(id string, session Session, c *C) {
	err := s.testCache.Add(id, session)
	c.Assert(err, IsNil)
}

// TestNewSessionCache tests a new session cache can be created
func (s *SessionCacheTestSuite) TestNewSessionCache(c *C) {
	// Given - No session cache exists

	// When -  An attempt is made to create a SessionCache
	cache := NewSessionCache()
	defer cache.Close()

	// Then - a new SessionCache is created
	c.Assert(cache, FitsTypeOf, &SessionCache{})
}

// TestSessionAdd tests a new session store can be created
func (s *SessionCacheTestSuite) TestAddSuccess(c *C) {
	// Given - A session is created
	id, session := newSession()

	// When -  A new session is added
	err := s.testCache.Add(id, session)

	// Then - Check the session is correctly added
	c.Assert(err, IsNil)
	storedSession, ok := s.testCache.(*SessionCache).store[id]
	c.Check(ok, Equals, true)
	c.Check(*storedSession, DeepEquals, session)
}

// TestAddSessionExists tests Add fails if the session with the provided
// ID already exists
func (s *SessionCacheTestSuite) TestAddSessionExists(c *C) {
	// Given - A single session added
	id, session := newSession()
	s.addSession(id, session, c)
	session.currentSum = 1

	// When -  A new session is added
	err := s.testCache.Add(id, session)

	// Then - Check the session is not added and the original session exists
	c.Assert(err, NotNil)
	storedSession, ok := s.testCache.(*SessionCache).store[id]
	c.Check(ok, Equals, true)
	c.Check(storedSession.currentSum, Equals, uint32(0))
}

// TestAddSessionExists tests Set can be used to update a session that
// has previously been stored
func (s *SessionCacheTestSuite) TestSetSuccess(c *C) {
	// Given - A single session added
	id, session := newSession()
	s.addSession(id, session, c)
	session.currentSum = 1

	// When -  Set is called to update the session
	err := s.testCache.Set(id, session)

	// Then - Check the session has been updated
	c.Assert(err, IsNil)
	storedSession, ok := s.testCache.(*SessionCache).store[id]
	c.Check(ok, Equals, true)
	c.Check(storedSession.currentSum, Equals, uint32(1))
}

// TestSetDoesNotExist tests Set fails if there is no Session stored for the
// provided key
func (s *SessionCacheTestSuite) TestSetDoesNotExist(c *C) {
	// Given - A single session added
	id, session := newSession()

	// When -  A the updated session is Set
	err := s.testCache.Set(id, session)

	// Then - Check the session is not added and the original session exists
	c.Assert(err, NotNil)
	_, ok := s.testCache.(*SessionCache).store[id]
	c.Check(ok, Equals, false)
}

// TestGetSuccess tests Get can be used to retrieve a previously stored session
func (s *SessionCacheTestSuite) TestGetSuccess(c *C) {
	// Given - A single session added
	id, session := newSession()
	s.addSession(id, session, c)

	// When -  Get is called to retrieve the session
	retrievedSession, err := s.testCache.Get(id)

	// Then - Check the session has been retrieved ok
	c.Assert(err, IsNil)
	c.Check(retrievedSession, DeepEquals, session)
}

// TestGetDoesNotExist tests that if Get is called with an ID that does not exist
// an error is returned
func (s *SessionCacheTestSuite) TestGetDoesNotExist(c *C) {
	// Given - No session added but an ID is generated
	id := uuid.New().String()

	// When -  Get is called to retrieve the session
	_, err := s.testCache.Get(id)

	// Then - Check the session has been retrieved ok
	c.Assert(err, NotNil)
}

// TestDeleteSuccess tests Delete can be used to remove a previously stored session
func (s *SessionCacheTestSuite) TestDeleteSuccess(c *C) {
	// Given - A single session added
	id, session := newSession()
	s.addSession(id, session, c)

	// When -  The session is deleted
	err := s.testCache.Delete(id)

	// Then - Check the session has been deleted ok
	c.Assert(err, IsNil)
	c.Check(len(s.testCache.(*SessionCache).store), Equals, 0)
}

// TestSetTTLSuccess tests SetTTL can be used to Set the time to live on
// a previously stored session
func (s *SessionCacheTestSuite) TestSetTTLSuccess(c *C) {
	// Given - A single session added
	id, session := newSession()
	s.addSession(id, session, c)

	// When -  When the session TTL is set
	err := s.testCache.SetTTL(id, 5*time.Second)

	// Then - Check the session TTL has been updated
	c.Assert(err, IsNil)
	storedSession, ok := s.testCache.(*SessionCache).store[id]
	c.Check(ok, Equals, true)
	c.Check(storedSession.expireAt, Not(DeepEquals), time.Time{})
}

// TestSetTTLDoesNotExist tests SetTTL returns an error if the session does not exist
func (s *SessionCacheTestSuite) TestSetTTLDoesNotExist(c *C) {
	// Given - A single session is created but not added
	id, _ := newSession()

	// When -  The session TTL is set
	err := s.testCache.SetTTL(id, 5*time.Second)

	// Then - Check SetTTL returns an error
	c.Assert(err, NotNil)
}

// TestSetTTLSuccessSessionExpired tests that when the TTL is set and the
// TTL time elapses the session is expired
func (s *SessionCacheTestSuite) TestSetTTLSuccessSessionExpired(c *C) {
	// Given - A single session added
	id, session := newSession()
	s.addSession(id, session, c)
	err := s.testCache.SetTTL(id, 1*time.Second)
	c.Assert(err, IsNil)

	// When -  When the session TTL time has elapsed
	time.Sleep(2 * time.Second)

	// Then - Check the session is expired
	session, err = s.testCache.Get(id)
	c.Assert(err, IsNil)
	c.Check(session.expired, Equals, true)
}

// TestNoTTLSessionNotExpired tests that when no TTL is set and a session
// is added and some time elapses the session is not expired
func (s *SessionCacheTestSuite) TestNoTTLSessionNotExpired(c *C) {
	// Given - A single session added
	id, session := newSession()
	s.addSession(id, session, c)

	// When -  When the session TTL is set
	time.Sleep(2 * time.Second)

	// Then - Check the session TTL has been updated
	_, err := s.testCache.Get(id)
	c.Assert(err, IsNil)
	c.Check(len(s.testCache.(*SessionCache).store), Equals, 1)
}
