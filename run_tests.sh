#!/usr/bin/env bash

set -e 

TESTIMAGE=testimage
TESTDIR="$(pwd)"

cd server
go test -gocheck.vv
cd -

docker build -f Dockerfile-test -t ${TESTIMAGE} .
docker run --rm --env TESTDIR=${TESTDIR} -v /var/run/docker.sock:/var/run/docker.sock -v "$(pwd)":/test ${TESTIMAGE}