package protocol

type Connect struct {
	ID             string `json:"id"`
	StartValue     int    `json:"start_value"`
	SequenceLength int    `json:"sequence_length"`
}

type Sequence struct {
	Value    int    `json:"value"`
	Checksum uint32 `json:"checksum"`
}
